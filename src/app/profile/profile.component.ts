import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../../services/service.service'

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  profile: any;
  uid: any;

  constructor(private service: ServiceService) { }

  ngOnInit() {

    this.uid = localStorage.getItem('uid');
    this.service.getProfile(this.uid).subscribe((res) => {
      console.log(res);
    });

  }

}
