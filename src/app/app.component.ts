import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'colab';
  constructor(private router: Router, ) { }
  ngOnInit(): void {
    let uid = localStorage.getItem('uid');
    if (uid && uid !== null) {
      this.router.navigate(['/home']);
    }

  }
}
