import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../../services/service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email: string;
  password: string;
  error: string = "";

  constructor(private service: ServiceService, private router: Router) { }

  ngOnInit() {
  }


  loginSubmit() {
    this.error = "";
    let data = { 'email': this.email, 'password': this.password };
    this.service.login(data).subscribe((res: any) => {
      console.log(res)
      // localStorage.setItem('uid', res.u);
      if (res.hasOwnProperty('user_id')) {
        localStorage.setItem('email', res.email);
        localStorage.setItem('lname', res.user_id);
        this.router.navigate(['/home']);
      } else {
        this.error = "Invalid credentials."
      }
    }, err => {
      console.log(err);
    })
  }
}
