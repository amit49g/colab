import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceService } from '../../services/service.service'

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  isDetails: boolean = false;
  isReply: boolean = false;
  posts: any = [];
  topics: any = [];
  constructor(private service: ServiceService, private router: Router) { }

  ngOnInit() {
    this.getAllPost();
    this.service.getTopics().subscribe((res: any) => {
      console.log(res);
      this.topics = res.topics;
    });
  }

  getPostDetails() {
    this.isDetails = true;
  }

  back() {
    this.isReply = false;
    this.isDetails = false;
  }
  reply() {
    this.isReply = true;
  }

  submitComment() {

  }

  getAllPost() {
    this.posts = [];
    this.service.getAllPosts().subscribe((res: any) => {
      console.log(res);
      this.posts = res;
    });
  }

  getPostOnTopic(topicid) {
    console.log(topicid);
    this.service.getPostsOnTopic(topicid).subscribe((res: any) => {
      console.log(res);
      this.posts = res;
    });
  }

}
