import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  url: string = "http://10.216.4.79:5005";

  constructor(private http: HttpClient) { }

  login(data) {
    return this.http.post(this.url + '/login', data);
  }

  getTopics() {
    return this.http.get(this.url + '/topics');
  }

  getProfile(id) {
    return this.http.get(this.url + '/profile?user_id=' + id);
  }

  getAllPosts() {
    return this.http.get(this.url + '/post');
  }
  getPostsOnTopic(topicid) {
    return this.http.get(this.url + '/post?topic_id=' + topicid);
  }
}
